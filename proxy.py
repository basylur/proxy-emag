import requests
import web
import json


endpoint = "https://m-api.emag.ro/v1.0"
headers = {"x-app-version": "android-1.5"}

urls = (
    '/product/(.+)' , 'product',
    '/cart/product' , 'addProduct',
    '/cart/process' , 'cartProcess',
    '/cart', 'cartPost',
    '/send-order', 'sendOrder'
)

app = web.application(urls, globals())
wsgi_app = app.wsgifunc()
getRequests = ['/product/',]


def extractParams(params, dictToIntersect):
    intersect = {}
    for item in params.keys():
        if dictToIntersect.has_key(item):
            intersect[item] = params[item]
    return intersect

def renderResponse(response):
    dict = json.loads(response.text)
    return dict

def respondToClient(response):
    return "success" \
        if response.status_code == 200 \
        else "failed"

def sendRequest(url, params = False):
    response = ''
    if url in getRequests:
        response = requests.get(endpoint + url, headers=headers)
    else:
        response = requests.post(endpoint + url, params, headers=headers)
    return response



def login(params):
    return renderResponse(sendRequest('/user/login', params))['tokens']

def obtainOfferId(params):
    if "part_number_key" in params \
            and len(params["part_number_key"]) > 4:
        return renderResponse(sendRequest("/product/" + params["part_number_key"]))["data"]["id"]
    raise app.notfound()




class sendOrder():
    def POST(self):
        params = web.input(_method='post')
        headers["x-tokens"] = login({'email': 'xoresq@gmail.com',
                                     'password': 'georgel#1'})
        offerId = obtainOfferId(params)
        sendRequest("/cart/product", {"product_id": offerId})
        cart = {'user_name': 'Bogdan Sorescu',
                'user_phone': '0721362432',
                'invoice_type': 'pf',
                'delivery_user_adress_id': '8125301',
                'delivery_2h': 'true',
                'payment_method': 'numerar',
                'user_phone_send_sms': 'true'}
        response = renderResponse(sendRequest('/cart/process', cart))
        if response['code'] == 400 and "2H" in response['notifications']['error'][0]['message']:
            cart['delivery_2h'] = 'false'
            response = renderResponse(sendRequest('/cart/process', cart))
        return response['status']


class product:
    def GET(self, product):
        params = web.input(_method='get')
        response = requests.get(endpoint + "/product/" + str(product),
                                headers=headers)
        return renderResponse(response)['data']['id']



class addProduct:
    def POST(self):
        params = web.input(_method='post')
        file = open('test', 'w')
        file.write(str(extractParams(params, Params.cart)))
        file.close()
        headers["x-tokens"] = params["tokens"]
        response = requests.post(endpoint + "/cart/product",
                                 params,
                                 headers=headers)
        return respondToClient(response)


class cartProcess:
    def POST(self):
        params = web.input(_method='post')
        headers["x-tokens"] = params["tokens"]
        response = requests.post(endpoint + "/cart/process",
                                 params,
                                 headers=headers)
        return respondToClient(response)


class cartPost:
    def POST(self):
        params = web.input(_method='post')
        headers["x-tokens"] = params["tokens"]
        response = requests.post(endpoint + "/cart",
                                 params,
                                 headers=headers)
        response = renderResponse(response)
        return "success" \
            if response['data']['empty'] != True \
               and response['data']['delivery_2h_options']  \
            else "failed"


if __name__ == "__main__":
    app.internalerror = web.debugerror
    app.run()